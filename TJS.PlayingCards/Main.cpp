
// Playing Cards
// Tyler Stearns
// Dakota Meisel

#include <iostream>;
#include <conio.h>;
#include <string>;
using namespace std;

enum  Suit
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

enum  Rank
{
	ACE = 14,
	KING = 13,
	QUEEN = 12,
	JACK = 11,
	TEN = 10,
	NINE = 9,
	EIGHT = 8,
	SEVEN = 7,
	SIX = 6,
	FIVE = 5,
	FOUR = 4,
	THREE = 3,
	TWO = 2
};

struct Card
{
	Rank Rank;
	Suit Suit;
};

string getRankString(Card c) {
	string ranks[] = { "none","none", "Two","three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King","Ace" };
	return ranks[c.Rank];
}

string getSuitString(Card c) {
	string suits[] = { "HEARTS","DIAMOND","CLUBS","SPADES" };
	return suits[c.Suit];
}

void PrintCard(Card card) {
	//Prints out the rank and suit of the card that is passed in
	cout << "Your card is the: " << getRankString(card) << " of " << getSuitString(card) << "\n";


}
Card highCard(Card c1, Card c2) {
	
	if (c1.Suit > c2.Suit)
	{
		return c1;
	}
	else if (c1.Suit < c2.Suit) {
		return c2;
	}
	if (c1.Rank > c2.Rank) {
		return c1;
	}
	else {
		return c2;
	}
}

int main()
{

	Card c1;
	c1.Rank = KING;
	c1.Suit = SPADES;

	PrintCard(c1);

	Card c2;
	c2.Rank = JACK;
	c2.Suit = HEARTS;
	PrintCard(c2);

	Card c3;
	c3 = highCard(c1, c2);
	PrintCard(c3);


	(void)_getch();
	return 0;
}
